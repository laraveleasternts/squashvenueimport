<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSomeColumnInNewVenuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_venues', function (Blueprint $table) {
            $table->text('membership_model')->nullable()->after('reject_reason');
            $table->text('membership_numbers')->nullable()->after('membership_model');
            $table->text('non_member_numbers')->nullable()->after('membership_numbers');
            $table->text('collect_member_contact_information')->nullable()->after('non_member_numbers');
            $table->text('most_profitable_area')->nullable()->after('collect_member_contact_information');
            $table->text('biggest_frustration')->nullable()->after('most_profitable_area');
            $table->text('support_from_sa_required')->nullable()->after('biggest_frustration');
            $table->text('comments_notes')->nullable()->after('support_from_sa_required');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_venues', function (Blueprint $table) {

            $table->dropColumn(['membership_model']);
            $table->dropColumn(['membership_numbers']);
            $table->dropColumn(['non_member_numbers']);
            $table->dropColumn(['collect_member_contact_information']);
            $table->dropColumn(['most_profitable_area']);
            $table->dropColumn(['biggest_frustration']);
            $table->dropColumn(['support_from_sa_required']);
            $table->dropColumn(['comments_notes']);
        });
    }
}
