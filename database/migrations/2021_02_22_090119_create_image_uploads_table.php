<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageUploadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_uploads', function (Blueprint $table) {
            $table->increments('id')->index()->unique()->comment('AUTO_INCREMENT');
            $table->text('file')->nullable()->comment('Images');
            $table->text('file_thumb_100')->nullable()->comment('resize and save thumbnail 100 * 100');
            $table->text('file_thumb_200')->nullable()->comment('resize and save thumbnail 200 * 200');
            $table->text('file_thumb_400')->nullable()->comment('resize and save thumbnail 400 * 400');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_uploads');
    }
}
