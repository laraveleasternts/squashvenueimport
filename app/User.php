<?php

namespace App;
use App\Events\ManageUser;
use App\Http\Resources\DataTrueResource;
use App\Http\Resources\User\UsersResource;
use App\Jobs\SendEmail;
use App\Models\User\Country;
use App\Models\User\Hobby;
use App\Models\User\Role;
use App\Models\User\State;
use App\Models\User\City;
use App\Models\User\UserGallery;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\Scopes;
use Laravel\Passport\HasApiTokens;
use App\Traits\UploadTrait;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,Scopes,HasApiTokens, SoftDeletes, UploadTrait;

    protected $table = 'users';

    public function scopeCommonFunctionMethod($query, $model, $request, $preQuery = null, $tablename = null, $groupBy = null, $export_select = false, $no_paginate = false)
    {
        return $this->getCommonFunctionMethod($model, $request, $preQuery, $tablename , $groupBy , $export_select , $no_paginate);
    }

    public static function getCommonFunctionMethod($model, $request, $preQuery = null, $tablename = null, $groupBy = null, $export_select = false, $no_paginate = false)
    {
        if (is_null($preQuery)) {
            $mainQuery = $model::withSearch($request->get('search'), $export_select);
        } else {
            $mainQuery = $model->withSearch($request->get('search'), $export_select);
        }
        if($request->filled('filter') != '')
            $mainQuery = $mainQuery->withFilter($request->get('filter'));
        if(!is_null($groupBy))
            $mainQuery = $mainQuery->groupBy($groupBy);
        if ( $no_paginate ){
            return $mainQuery->withOrderBy($request->get('sort'), $request->get('order_by'), $tablename, $export_select);
        }else{

            $mainQuery = $mainQuery->withOrderBy($request->get('sort'), $request->get('order_by'), $tablename, $export_select);

            if($request->filled('per_page'))
                return $mainQuery->withPerPage($request->get('per_page'));
            else
                return $mainQuery->withPerPage($mainQuery->count());

        }
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'mobile_no', 'user_type', 'role_id', 'profile', 'gender', 'dob', 'address','country_id','state_id','city_id', 'status', 'email_verified_at', 'remember_token'
    ];

    /**
     * Lightweight response variable
     *
     * @var array
     */
    public $light = [
        'id', 'name', 'email'
    ];

    /**
     * @var array
     */
    public $sortable=[
        'name', 'email', 'mobile_no', 'gender', 'dob', 'address'
    ];

    public $foreign_sortable = [
        'country_id','state_id','city_id','role_id'
    ];

    public $foreign_table = [
        'countries','states','cities','roles'
    ];

    public $foreign_key = [
        'name','name','name','name'
    ];

    public $foreign_method = [
        'country','state','city','role'
    ];

    public $type_sortable = [
        'status',
        'user_type'
    ];

    public $type_enum = [
        [
            'constants.user.status_enum.0',
            'constants.user.status_enum.1',
        ],
        [
            'constants.user.user_type_enum.0',
            'constants.user.user_type_enum.1',
        ],
    ];
    public $type_enum_text = [
        [
            'constants.user.status.0',
            'constants.user.status.1',
        ],
        [
            'constants.user.user_type.0',
            'constants.user.user_type.1',
        ],
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['email_verified_at', 'created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id'=>'string',
        'name'=>'string',
        'email'=>'string',
        'password'=>'string',
        'mobile_no'=>'string',
        'user_type'=>'string',
        'role_id'=>'string',
        'profile'=>'string',
        'gender'=>'string',
        'dob'=>'string',
        'address'=>'string',
        'country_id'=>'string',
        'state_id'=>'string',
        'city_id'=>'string',
        'status'=>'string',
        'email_verified_at'=>'string',
        'created_at'=>'string',
        'updated_at'=>'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country() {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state() {
        return $this->belongsTo(State::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city() {
        return $this->belongsTo(City::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_galleries() {
        return $this->hasMany(UserGallery::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function hobbies() {
        return $this->belongsToMany(Hobby::class,"hobby_users","user_id","hobby_id");
    }

    /**
     * @param $value
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function getProfileAttribute($value){
        if ($value == NULL)
            return "";
        return url(config('constants.image.dir_path') . $value);
    }

    /**
     * Common Display Error Message.
     *
     * @param $query
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function GetError($message){
        return response()->json(['error' => $message], config('constants.validation_codes.unprocessable_entity'));
    }

    public function scopeRegister($query,$request,$userType){

        $checkEmailExist = $this->checkEmail($request,$userType);
        if($checkEmailExist)
            return User::GetError(config('constants.messages.email_already_exist_error'));

        $data = $request->all();

        if($userType == config('constants.user.user_type_enum.0')){// Admin user type

            $data['status'] = config('constants.user.status_enum.1');
            $data['email_verified_at'] = config('constants.calender.date_time');

        }

        $data['password'] = bcrypt($data['password']);
        $data['user_type'] = $userType;
        $user = User::create($data);

        $realPath = 'user/' . $user->id.'/';
        if($request->hasfile('profile')) {
            $path = $this->uploadOne($request->file('profile'), '/public/'.$realPath);
            $user->update(['profile' => $realPath.pathinfo($path,PATHINFO_BASENAME)]);
        }

        if($request->hasfile('gallery')) {
            foreach ($request->gallery as $image) {
                $path = $this->uploadOne($image, '/public/'.$realPath);
                UserGallery::create(['user_id' => $user->id, 'filename' =>  $realPath.pathinfo($path,PATHINFO_BASENAME)]);
            }
        }

        if($data['hobby']) {
            $user->hobbies()->attach($data['hobby']); //this executes the insert-query
        }

        event(new ManageUser($user,config('constants.broadcasting.operation.add')));

        if($userType == config('constants.user.user_type_enum.1')){// User create from frontend
            SendEmail::dispatch($user);
            return response()->json(['data' => config('constants.messages.registration_success')], config('constants.validation_codes.ok'));
        }
        else
            return new UsersResource($user);
    }

    /**
     * Single Delete user and it's details
     *
     * @param $query
     * @param $user
     */
    public function scopeDeleteSingle($query,$user)
    {
        $user->hobbies()->detach();

        $realPath = 'user/' . $user->id.'/';
        Storage::deleteDirectory('/public/'.$realPath);
        UserGallery::where('user_id', $user->id)->delete();

        Storage::deleteDirectory('/public/'.$realPath);
        $user->delete();
    }

    /**
     * Multiple Delete
     * @param $query
     * @param $request
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     */
    public function scopeDeleteAll($query,$request){
        if(!empty($request->id)) {

            if(in_array(config('constants.system_user_id'),$request->id))
                return User::GetError(config('constants.messages.admin_user_delete_error'));

            //todo please uncomment this code IF you validate user multiple delete for created_by and updated_by column
            /*$inUse = User::commonCodeForDeleteModelRestrictions([Role::class,Country::class,State::class,City::class,Hobby::class,Permission::class],'created_by',$request->id);
            if(!empty($inUse))
                return User::GetError("User is unable to delete because it's used in [".implode(",",$inUse)."].");

            $inUse = User::commonCodeForDeleteModelRestrictions([Role::class,Country::class,State::class,City::class,Hobby::class,Permission::class],'updated_by',$request->id);
            if(!empty($inUse))
                return User::GetError("User is unable to delete because it's used in [".implode(",",$inUse)."].");*/

            foreach ($request->id as $userId){

                $user = User::where('id',$userId)->with('hobbies')->first();
                if($user){
                    $this->deleteSingle($user);
                }

            }

            event(new ManageUser($request->id,config('constants.broadcasting.operation.delete_multiple')));

            return new DataTrueResource(true);
        }
        else{
            return User::GetError(config('constants.messages.delete_multiple_error'));
        }
    }

    /**
     * update User
     *
     * @param $query
     * @param $request
     * @param $user
     * @return UsersResource|\Illuminate\Http\JsonResponse
     */
    public function scopeUpdateUser($query,$request,$user){

        $checkEmailExist = $this->checkEmail($request,$request->get('user_type'),$user->id);
        if($checkEmailExist)
            return User::GetError(config('constants.messages.email_already_exist_error'));

        if ( $request->hasFile('gallery') ){

            $totalImages = $this->calculateImageCriteria($request,$user->id,count($request->gallery));// Calculate image criteria for image validation

            if($totalImages > config('constants.max_user_gallery'))// Check maximum 5 images are valid to upload
                return User::GetError(config('constants.messages.user_max_image_upload_error'));

        }

        $data = $request->all();
        $realPath = 'user/' . $user->id.'/';
        if($request->hasfile('profile')) {
            $this->deleteOne('/public/'.$realPath . '/' . basename($user->profile));
            $path = $this->uploadOne($request->file('profile'), '/public/'.$realPath);
            $data['profile'] = $realPath.pathinfo($path,PATHINFO_BASENAME);
        }

        if($request->hasfile('gallery')) {
            foreach ($request->gallery as $image) {
                $path = $this->uploadOne($image, '/public/'.$realPath);
                UserGallery::create(['user_id' => $user->id, 'filename' =>  $realPath.pathinfo($path,PATHINFO_BASENAME)]);
            }
        }

        if($data['hobby']) {
            $user->hobbies()->detach(); //this executes the delete-query
            $user->hobbies()->attach($data['hobby']); //this executes the insert-query
        }

        $user->update($data);

        event(new ManageUser($user,config('constants.broadcasting.operation.edit')));

        return new UsersResource($user);
    }

    /**
     *
     *
     * @param $request
     * @param int $productId - Default should be 0
     * @return array
     */

    /**
     * This method is used to calculate image validation criteria for max file uploads
     *
     * @param $request
     * @param int $userId
     * @return int
     */
    public function calculateImageCriteria($request, $userId, $totalImages)
    {
        $userImages = UserGallery::where('user_id',$userId)->get();

        if(!$userImages->isEmpty()){

            foreach ($userImages as $userImage){

                $totalImages++;
            }

        }

        return $totalImages;

    }

    public function checkEmail($request,$userType,$userId = 0)
    {
        $user = User::where('email',$request->get('email'))
            ->where('user_type',$userType)->first();

        if($user){

            if($userId != 0){

                if($userId != $user->id)
                    return true;

            }
            else
                return true;

        }

        return false;
    }
}
