<?php

namespace App\Http\Controllers\API\User;

use App\Events\ManageState;
use App\Exports\User\StatesExport;
use App\Http\Resources\DataTrueResource;
use App\Models\User\City;
use App\User;
use App\Models\User\State;
use App\Http\Requests\User\StatesRequest;
use App\Http\Resources\User\StatesCollection;
use App\Http\Resources\User\StatesResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

/*
   |--------------------------------------------------------------------------
   | States Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles the Roles of
       index,
       show,
       store,
       update,
       destroy,
       export and
       importBulk Methods.
   |
   */

class StatesAPIController extends Controller
{
    /**
     * List States
     * @param Request $request
     * @return StatesCollection
     */
    public function index(Request $request)
    {
        if ($request->get('is_light', false)) {
            $state = new State();
            $query = User::commonFunctionMethod(State::select($state->light), $request, true);
        } else
            $query = User::commonFunctionMethod(State::with(['country']), $request, true);

        return new StatesCollection(StatesResource::collection($query), StatesResource::class);
    }

    /**
     * States Detail
     * @param State $state
     * @return StatesResource
     */
    public function show(State $state)
    {
        return new StatesResource($state->load([]));
    }

    /**
     * Add State
     * @param StatesRequest $request
     * @return StatesResource
     */
    public function store(StatesRequest $request)
    {
        $state = State::create($request->all());
        event(new ManageState($state, config('constants.broadcasting.operation.add')));
        return new StatesResource($state);
    }

    /**
     * Update State
     * @param StatesRequest $request
     * @param State $state
     * @return StatesResource
     */
    public function update(StatesRequest $request, State $state)
    {
        $state->update($request->all());
        event(new ManageState($state, config('constants.broadcasting.operation.edit')));
        return new StatesResource($state);
    }

    /**
     * Delete State
     *
     * @param Request $request
     * @param State $state
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, State $state)
    {
        $inUse = State::commonCodeForDeleteModelRestrictions([User::class, City::class], 'state_id', [$state->id]);
        if (!empty($inUse))
            return User::GetError("State is unable to delete because it's used in [" . implode(",", $inUse) . "].");

        $state->delete();
        event(new ManageState($state, config('constants.broadcasting.operation.delete')));
        return new DataTrueResource($state);
    }

    /**
     * Delete State multiple
     * @param Request $request
     * @return DataTrueResource
     */
    public function deleteAll(Request $request)
    {
        return State::deleteAll($request);
    }
    /**
     * Export States Data
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        return Excel::download(new StatesExport($request), 'state.csv');
    }
}
