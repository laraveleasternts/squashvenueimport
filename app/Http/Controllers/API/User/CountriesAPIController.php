<?php

namespace App\Http\Controllers\API\User;

use App\Events\ManageCountry;
use App\Exports\User\CountriesExport;
use App\Http\Resources\DataTrueResource;
use App\Imports\User\CountriesImport;
use App\Imports\User\SquashImport;
use App\Models\User\State;
use App\User;
use App\Models\User\Country;
use App\Http\Requests\User\CountriesRequest;
use App\Http\Resources\User\CountriesCollection;
use App\Http\Resources\User\CountriesResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

/*
 |--------------------------------------------------------------------------
 | Countries Controller
 |--------------------------------------------------------------------------
 |
 | This controller handles the Roles of
   index,
   show,
   store,
   update,
   destroy,
   export and
   importBulk Methods.
 |
 */
class CountriesAPIController extends Controller
{
    /**
     * list Countires
     * @param Request $request
     * @return CountriesCollection
     */
    public function index(Request $request)
    {
        if($request->get('is_light',false)){
            $country = new Country();
            $query = User::commonFunctionMethod(Country::select($country->light),$request,true);
        }
        else
            $query = User::commonFunctionMethod(Country::class,$request);

        return new CountriesCollection(CountriesResource::collection($query),CountriesResource::class);
    }

    /**
     * Country Detail
     * @param Country $country
     * @return CountriesResource
     */
    public function show(Country $country)
    {
        return new CountriesResource($country->load([]));
    }

    /**
     * Add Country
     * @param CountriesRequest $request
     * @return CountriesResource
     */
    public function store(CountriesRequest $request)
    {
       // dd(123);
        $country = Country::create($request->all());
        event(new ManageCountry($country,config('constants.broadcasting.operation.add')));
        return new CountriesResource($country);
    }

    /**
     * Update Country
     * @param CountriesRequest $request
     * @param Country $country
     * @return CountriesResource
     */
    public function update(CountriesRequest $request, Country $country)
    {
        $country->update($request->all());
        event(new ManageCountry($country,config('constants.broadcasting.operation.edit')));
        return new CountriesResource($country);
    }

    /**
     * Delete Country
     *
     * @param Request $request
     * @param Country $country
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, Country $country)
    {
        $inUse = Country::commonCodeForDeleteModelRestrictions([User::class,State::class],'country_id',[$country->id]);
        if(!empty($inUse))
            return User::GetError("Country is unable to delete because it's used in [".implode(",",$inUse)."].");

        $country->delete();
        event(new ManageCountry($country,config('constants.broadcasting.operation.delete')));

        return new DataTrueResource($country);
    }

    /**
     * Delete Country multiple
     * @param Request $request
     * @return DataTrueResource
     */
    public function deleteAll(Request $request)
    {
        return Country::deleteAll($request);
    }
    /**
     * Export Country Data
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        return Excel::download(new CountriesExport($request), 'country.csv');
    }

    /**
     * Import bulk
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importBulk(Request $request)
    {
        return User::importBulk($request,new CountriesImport(),config('constants.models.country_model'),config('constants.import_dir_path.country_dir_path'));
    }
    public function squashImportBulk(Request $request)
    {
        return User::importBulk($request,new SquashImport(),config('constants.models.country_model'),config('constants.import_dir_path.country_dir_path'));
    }
}
