<?php

namespace App\Http\Controllers\API\User;

use Illuminate\Http\Request;
use App\Models\User\ImageUpload;
use File;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;

class ImageUploadController extends Controller{
    public $imagesPath = '';
    public $thumbnailPath_100 = '';
    public $thumbnailPath_200 = '';
    public $thumbnailPath_400 = '';

    /**
     * @function CreateDirectory
     * create required directory if not exist and set permissions
     */
    public function createDirecrotory()
    {
        $paths = [
            'image_path' => storage_path('images/'),
            'thumbnailPath_100' => storage_path('images/thumbs_100/'),
            'thumbnailPath_200' => storage_path('images/thumbs_200/'),
            'thumbnailPath_400' => storage_path('images/thumbs_400/')
        ];
        foreach ($paths as $key => $path) {
            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0755, true, true);
            }
        }
        $this->imagesPath = $paths['image_path'];
        $this->thumbnailPath_100 = $paths['thumbnailPath_100'];
        $this->thumbnailPath_200 = $paths['thumbnailPath_200'];
        $this->thumbnailPath_400 = $paths['thumbnailPath_400'];
    }

    /**
     * Post upload form
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postUploadForm(Request $request)
    {
        $request->validate([
            'upload.*' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        if($request->hasFile('upload')) {
            $this->createDirecrotory();
//            dd($request->upload);

            foreach ($request->upload as $file) {
                dd($file);

                $image = Image::make($file);
                // you can also use the original name
                $imageName = time().'-'.$file->getClientOriginalName();
                // save original image
                $image->save($this->imagesPath.$imageName);
                // resize and save thumbnail 100 * 100
                $image->resize(100,100);
                $image->save($this->thumbnailPath_100.$imageName);

                // resize and save thumbnail 200 * 200
                $image->resize(200,200);
                $image->save($this->thumbnailPath_200.$imageName);

                // resize and save thumbnail 400 * 400
                $image->resize(400,400);
                $image->save($this->thumbnailPath_400.$imageName);

                $upload = new ImageUpload();
                $upload->file = 'images/' . pathinfo($imageName, PATHINFO_BASENAME);
                $upload->file_thumb_100 = 'images/thumbs_100/' . pathinfo($imageName, PATHINFO_BASENAME);
                $upload->file_thumb_200 = 'images/thumbs_200/' . pathinfo($imageName, PATHINFO_BASENAME);
                $upload->file_thumb_400 = 'images/thumbs_400/' . pathinfo($imageName, PATHINFO_BASENAME);
                $upload->save();

            }
            return back()->with('success', 'Your images has been successfully Upload.');
        }
    }

    public function generateCustomImage(Request $request, ImageUpload $image)
    {
        $path = public_path('images/thumbs_custom/');

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0755, true, true);
        }

        $imageMaker = Image::make($image->file);

        $ext = pathinfo($image->file, PATHINFO_EXTENSION);

        $imageName = time().'.'.$ext;

        $imageMaker->resize($request->get('width'),$request->get('height'));
        $imageMaker->save($path.$imageName);

        $image->custom_size = 'images/thumbs_custom/' . pathinfo($imageName, PATHINFO_BASENAME);
        $image->save();

        return redirect()->to('/upload-image');
    }
}
