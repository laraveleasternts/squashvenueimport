<?php

namespace App\Http\Controllers\API\User;

use App\Events\ManageHobby;
use App\Exports\User\HobbiesExport;
use App\Http\Resources\DataTrueResource;
use App\Models\User\Hobby_user;
use App\User;
use App\Models\User\Hobby;
use App\Http\Requests\User\HobbiesRequest;
use App\Http\Resources\User\HobbiesCollection;
use App\Http\Resources\User\HobbiesResource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;

/*
   |--------------------------------------------------------------------------
   | Hobbies Controller
   |--------------------------------------------------------------------------
   |
   | This controller handles the Roles of
       index,
       show,
       store,
       update,
       destroy,
       export and
       importBulk Methods.
   |
   */

class HobbiesAPIController extends Controller
{
    /**
     * Hobbies List
     * @param Request $request
     * @return HobbiesCollection
     */
    public function index(Request $request)
    {
        if($request->get('is_light',false)){
            $hobby = new Hobby();
            $query = User::commonFunctionMethod(Hobby::select($hobby->light),$request,true);
        }
        else
            $query = User::commonFunctionMethod(Hobby::class,$request);

        return new HobbiesCollection(HobbiesResource::collection($query),HobbiesResource::class);
    }

    /**
     * Hobby Detail
     * @param Hobby $hobby
     * @return HobbiesResource
     */
    public function show(Hobby $hobby)
    {
        return new HobbiesResource($hobby->load([]));
    }

    /**
     * Add Hobby
     * @param HobbiesRequest $request
     * @return HobbiesResource
     */
    public function store(HobbiesRequest $request)
    {
        $hobby = Hobby::create($request->all());
        event(new ManageHobby($hobby,config('constants.broadcasting.operation.add')));
        return new HobbiesResource($hobby);
    }

    /**
     * Update Hobby
     * @param HobbiesRequest $request
     * @param Hobby $hobby
     * @return HobbiesResource
     */
    public function update(HobbiesRequest $request, Hobby $hobby)
    {
        $hobby->update($request->all());
        event(new ManageHobby($hobby,config('constants.broadcasting.operation.edit')));
        return new HobbiesResource($hobby);
    }

    /**
     * Delete Hobby
     *
     * @param Request $request
     * @param Hobby $hobby
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Request $request, Hobby $hobby)
    {
        $inUse = Hobby::commonCodeForDeleteModelRestrictions([Hobby_user::class],'hobby_id',[$hobby->id]);
        if(!empty($inUse))
            return User::GetError("Hobby is unable to delete because it's used in [".implode(",",$inUse)."].");

        $hobby->delete();
        event(new ManageHobby($hobby,config('constants.broadcasting.operation.delete')));
        return new DataTrueResource($hobby);
    }

    /**
     * Delete Hobby multiple
     * @param Request $request
     * @return DataTrueResource
     */
    public function deleteAll(Request $request)
    {
        return Hobby::deleteAll($request);
    }
    /**
     * Export Hobbies Data
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(Request $request)
    {
        return Excel::download(new HobbiesExport($request), 'hobby.csv');
    }

    /**
     * Import bulk
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function importBulk(Request $request)
    {
        return User::importBulk($request,new HobbiesImport(),config('constants.models.hobby_model'),config('constants.import_dir_path.hobby_dir_path'));
    }
}
