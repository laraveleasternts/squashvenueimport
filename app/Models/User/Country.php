<?php

namespace App\Models\User;
use App\Events\ManageCountry;
use App\Http\Resources\DataTrueResource;
use App\Traits\Scopes;
use App\Traits\CreatedbyUpdatedby;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    use SoftDeletes, Scopes,CreatedbyUpdatedby;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $sortable=[
        'name',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'created_by','updated_by'
    ];

    /**
     * Lightweight response variable
     *
     * @var array
     */
    public $light = [
        'id', 'name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
        'id'=>'string',
        'name'=>'string'
    ];

    /**
     * Delete Multiple
     * @param $query
     * @param $request
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     */
    public function scopeDeleteAll($query,$request){
        if(!empty($request->id)) {

            $inUse = Country::commonCodeForDeleteModelRestrictions([User::class,State::class],'country_id',$request->id);
            if(!empty($inUse))
                return User::GetError("Country is unable to delete because it's used in [".implode(",",$inUse)."].");

            Country::whereIn('id', $request->id)->delete();

            event(new ManageCountry($request->id,config('constants.broadcasting.operation.delete_multiple')));

            return new DataTrueResource(true);
        }
        else{
            return User::GetError(config('constants.messages.delete_multiple_error'));
        }
    }
}
