<?php

namespace App\Models\User;

use App\Events\ManageRole;
use App\Http\Resources\DataTrueResource;
use App\Traits\Scopes;
use App\Traits\CreatedbyUpdatedby;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Model
{
    use Scopes,SoftDeletes,CreatedbyUpdatedby;

    //public $timestamps = false;
    public $sortable=[
        'name'
    ];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'guard_name', 'landing_page'
    ];

    /**
     * Lightweight response variable
     *
     * @var array
     */
    public $light = [
        'id', 'name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        //
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        //
        'id' =>'string',
    ];

    /**
     * Get the Users for the Role.
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get the Permissions for the Role.
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,"permission_roles","role_id","permission_id");
    }

    /**
     * Multiple Delete
     * @param $query
     * @param $request
     * @return DataTrueResource|\Illuminate\Http\JsonResponse
     */
    public function scopeDeleteAll($query,$request){
        if(!empty($request->id)) {

            $inUse = Role::commonCodeForDeleteModelRestrictions([User::class,Permission_role::class],'role_id',$request->id);
            if(!empty($inUse))
                return User::GetError("Role is unable to delete because it's used in [".implode(",",$inUse)."].");

            if(in_array(config('constants.system_role_id'),$request->id))
                return User::GetError(config('constants.messages.admin_role_delete_error'));

            Role::whereIn('id', $request->id)->delete();
            event(new ManageRole($request->id,config('constants.broadcasting.operation.delete_multiple')));

            return new DataTrueResource(true);
        }
        else{
            return User::GetError(config('constants.messages.delete_multiple_error'));
        }
    }
}
