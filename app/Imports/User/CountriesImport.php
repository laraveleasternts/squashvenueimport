<?php

namespace App\Imports\User;

use App\Models\New_venues;
use App\Models\Venue;
use App\Traits\Scopes;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Http;


class CountriesImport implements ToCollection, WithStartRow
{
    use Scopes;

    private $errors = [];
    private $rows = 0;

    public function startRow(): int
    {
        return 2;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function collection(Collection $collection)
    {
        $counter = 0;
        foreach ($collection as $venue) {
            dd(315);
            $input = urlencode($venue[1]);
            $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . $input . "&key=AIzaSyC6Ebl-s6AVB5jcYshKN-343Ld9wR-as2A";
            $response = Http::get($url);
            $content = json_decode($response);

            if ($content->status == "OK") {
                $predictions = $content->predictions;
                $row_count = count((array)$predictions);
                if ($row_count == '1') {
                    $place_id = $predictions[0]->place_id;
                    $place = New_venues::where('g_place_id', $place_id)->first();
//                    dd($place);
                    if ($place && $place->no_of_courts == null) {
                        $place->no_of_courts = $venue[13];
                        $place->no_of_glass_courts = $venue[15];
                        $place->no_of_non_glass_courts = $venue[16];
                        $place->center_type = $venue[4];
                          $place->managers_name = $venue[8];
                        $place->update();
                        echo ($counter + 1) . ". " . $venue[1] . " - Common result <br/>";
                        $counter++;

                    } else {
                        $data_result = New_venues::create([
                            'g_place_id' => $place_id,
                            'email' => $venue[10],
                            'court_rental_fees' => $venue[18],
                            'membership_model' => $venue[17],
                            'membership_numbers' => $venue[19],
                            'non_member_numbers' => $venue[20],
                            'collect_member_contact_information' => $venue[21],
                            'most_profitable_area' => $venue[22],
                            'biggest_frustration' => $venue[23],
                            'support_from_sa_required' => $venue[24],
                            'comments_notes' => $venue[25],
                            'center_type' => $venue[4],
                            'managers_name' => $venue[8],
                        ]);

                        $google_api_key = "AIzaSyC6Ebl-s6AVB5jcYshKN-343Ld9wR-as2A";

                        $client = new Client();
                        try {
                            //print_r($data_result->id . ' ');
                            $res = $client->get('https://maps.googleapis.com/maps/api/place/details/json?key=' . $google_api_key . '&place_id=' . $data_result->g_place_id);
                            if ($res->getStatusCode() == 200) {
                                // body -> https://developers.google.com/places/web-service/details#PlaceDetailsResults
                                $body = $res->getBody();
                                $body = json_decode($body);
                                if (isset($body->result)) {
                                    $result = $body->result;
                                    if (isset($result->business_status)
                                        && strtoupper($result->business_status) == strtoupper("CLOSED_PERMANENTLY")) {
                                        $data_result->permanently_closed = '1'; // set the flag to '1' for venue close
                                    }
                                    //if created_by is NULL than set it to admin (webmaster email id) whose id is 0
                                    if (is_null($data_result->created_by)) {
                                        $data_result->created_by = '1';
                                    }
                                    if (isset($result->formatted_address) && strlen($result->formatted_address) > 0) {
                                        $data_result->physical_address = $result->formatted_address;
                                    }
                                    if (isset($result->international_phone_number) && strlen($result->international_phone_number) > 0) {
                                        $data_result->telephone = $result->international_phone_number;
                                    }
                                    if (isset($result->name) && strlen($result->name) > 0) {
                                        $data_result->name = $result->name;
                                    }
                                    if (isset($result->url) && strlen($result->url) > 0) {
                                        $data_result->g_map_url = $result->url;
                                    }
                                    if (isset($result->website) && strlen($result->website) > 0) {
                                        $data_result->website = $result->website;
                                    }
                                    if (isset($result->types)) {
                                        $types = $result->types;
                                        $data_result->types = implode(';', $types);
                                    }
                                    //set the latitude & longitude
                                    if (isset($result->geometry)) {
                                        if (isset($result->geometry->location)) {
                                            $location = $result->geometry->location;
                                            if (isset($location->lat) && strlen($location->lat) > 0) {
                                                $data_result->latitude = $location->lat;
                                            }
                                            if (isset($location->lng) && strlen($location->lng) > 0) {
                                                $data_result->longitude = $location->lng;
                                            }
                                            if (isset($location->lat) && isset($location->lng)) {
                                                $res2 = $client->get('https://maps.googleapis.com/maps/api/elevation/json?key=' . $google_api_key . '&locations=' . $location->lat . ',' . $location->lng);
                                                if ($res2->getStatusCode() == 200) {

                                                    // body2 -> https://developers.google.com/maps/documentation/elevation/start
                                                    $body2 = $res2->getBody();
                                                    $body2 = json_decode($body2);

                                                    if (isset($body2->results)) {
                                                        foreach ($body2->results as $results2) {
                                                            $data_result->elevation = $results2->elevation;
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //set the country, country_code, state & suburb
                                    if (isset($result->address_components)) {
                                        $address_components = $result->address_components;
                                        foreach ($address_components as $address_component) {
                                            $types = $address_component->types;

                                            foreach ($types as $type) {
                                                // dont sync country, state & suburb for UK (England, Northern Ireland, Scotland, Wales) & Ireland
                                                if (($data_result->country_code != 'GB')
                                                    && ($data_result->country_code != 'IE')) {
                                                    if ($type == 'country') {
                                                        $data_result->country = $address_component->long_name;
                                                        $data_result->country_code = $address_component->short_name;
                                                        break;
                                                    }
                                                    if ($type == 'administrative_area_level_1') {
                                                        $data_result->state = $address_component->long_name;
                                                        break;
                                                    }
                                                    if ($type == 'postal_code') {
                                                        $data_result->postal_address = $address_component->long_name;
                                                        break;
                                                    }
                                                    if (($type == 'locality')
                                                        || ($type == 'postal_town')
                                                        || ($type == 'administrative_area_level_3')) {
                                                        $data_result->suburb = $address_component->long_name;
                                                        break;
                                                    }
                                                }
                                            } // end of types foreach
                                        } // end of address_components foreach
                                    }
                                    if (isset($result->photos)) {
                                        $photos = $result->photos;
                                        foreach ($photos as $photo) {
                                            $width = $photo->width;
//                                $height = $photo->height;
                                            $photo_reference = $photo->photo_reference;

                                            $data_result->venue_image = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $width . '&photoreference=' . $photo_reference . '&key=' . $google_api_key;

                                            break; //capture only the first object
                                        }
                                    }
                                    $data_result->last_sync_at = Carbon::now()->toDateTimeString();
                                    $data_result->update();
                                } //if body found
                            } // if status == 200

                        } catch (\Exception $e) {
                            print_r($e);
                        }
                    }
                } else {
                    echo "* " . $venue[1] . " - multiple result <br/>  ";
                }
            } else if ($content->status == "ZERO_RESULTS") {
                echo $venue[1] . "- zero result<br/>";
            }

            $this->rows++;
        }
    }

//}

    public function getRowCount(): int
    {
        return $this->rows;
    }
}
