<?php

namespace App\Imports\User;

use App\Models\Venue;
use App\Traits\Scopes;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Facades\Http;


class SquashImport implements ToCollection, WithStartRow
{
    use Scopes;

    private $errors = [];
    private $rows = 0;

    public function startRow(): int
    {
        return 2;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function collection(Collection $collection)
    {
        foreach ($collection as $venue) {
            $input = urlencode($venue[0]);
            $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" . $input . "&key=AIzaSyC6Ebl-s6AVB5jcYshKN-343Ld9wR-as2A";
            $response = Http::get($url);
            $content = json_decode($response);

            if ($content->status == "OK") {
                $predictions = $content->predictions;
                $row_count = count((array)$predictions);
                if ($row_count == '1') {
                    $place_id = $predictions[0]->place_id;

                    $data_result = Venue::create([
                        'g_place_id' => $place_id
                    ]);

                    $google_api_key = "AIzaSyC6Ebl-s6AVB5jcYshKN-343Ld9wR-as2A";

                    $client = new Client();
                    try {
                        //print_r($data_result->id . ' ');
                        $res = $client->get('https://maps.googleapis.com/maps/api/place/details/json?key=' . $google_api_key . '&place_id=' . $data_result->g_place_id);
                        if ($res->getStatusCode() == 200) {
                            // body -> https://developers.google.com/places/web-service/details#PlaceDetailsResults
                            $body = $res->getBody();
                            $body = json_decode($body);
                            if (isset($body->result)) {
                                $result = $body->result;
                                if (isset($result->business_status)
                                    && strtoupper($result->business_status) == strtoupper("CLOSED_PERMANENTLY")) {
                                    $data_result->permanently_closed = '1'; // set the flag to '1' for venue close
                                }
                                //if created_by is NULL than set it to admin (webmaster email id) whose id is 0
                                if (is_null($data_result->created_by)) {
                                    $data_result->created_by = '1';
                                }
                                if (isset($result->formatted_address) && strlen($result->formatted_address) > 0) {
                                    $data_result->physical_address = $result->formatted_address;
                                }
                                if (isset($result->international_phone_number) && strlen($result->international_phone_number) > 0) {
                                    $data_result->telephone = $result->international_phone_number;
                                }
                                if (isset($result->name) && strlen($result->name) > 0) {
                                    $data_result->name = $result->name;
                                }
                                if (isset($result->url) && strlen($result->url) > 0) {
                                    $data_result->g_map_url = $result->url;
                                }
                                if (isset($result->website) && strlen($result->website) > 0) {
                                    $data_result->website = $result->website;
                                }
                                if (isset($result->types)) {
                                    $types = $result->types;
                                    $data_result->types = implode(';', $types);
                                }
                                //set the latitude & longitude
                                if (isset($result->geometry)) {
                                    if (isset($result->geometry->location)) {
                                        $location = $result->geometry->location;
                                        if (isset($location->lat) && strlen($location->lat) > 0) {
                                            $data_result->latitude = $location->lat;
                                        }
                                        if (isset($location->lng) && strlen($location->lng) > 0) {
                                            $data_result->longitude = $location->lng;
                                        }
                                        if(isset($location->lat) && isset($location->lng)){
                                            $res2 = $client->get('https://maps.googleapis.com/maps/api/elevation/json?key=' . $google_api_key . '&locations=' . $location->lat . ',' . $location->lng);
                                            if ($res2->getStatusCode() == 200) {
                                                // body2 -> https://developers.google.com/maps/documentation/elevation/start
                                                $body2 = $res2->getBody();
                                                $body2 = json_decode($body2);
                                                if (isset($body2->results)) {
                                                    foreach($body2->results as $results2){
                                                        $data_result->elevation = $results2->elevation;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //set the country, country_code, state & suburb
                                if (isset($result->address_components)) {
                                    $address_components = $result->address_components;
                                    foreach ($address_components as $address_component) {
                                        $types = $address_component->types;
                                        foreach ($types as $type) {
                                            // dont sync country, state & suburb for UK (England, Northern Ireland, Scotland, Wales) & Ireland
                                            if (($data_result->country_code != 'GB')
                                                && ($data_result->country_code != 'IE')) {
                                                if ($type == 'country') {
                                                    $data_result->country = $address_component->long_name;
                                                    $data_result->country_code = $address_component->short_name;
                                                    break;
                                                }
                                                if ($type == 'administrative_area_level_1') {
                                                    $data_result->state = $address_component->long_name;
                                                    break;
                                                }
                                                if ($type == 'postal_code') {
                                                    $data_result->postal_address = $address_component->long_name;
                                                    break;
                                                }
                                                if (($type == 'locality')
                                                    || ($type == 'postal_town')
                                                    || ($type == 'administrative_area_level_3')) {
                                                    $data_result->suburb = $address_component->long_name;
                                                    break;
                                                }
                                            }
                                        } // end of types foreach
                                    } // end of address_components foreach
                                }
                                if (isset($result->photos)) {
                                    $photos = $result->photos;
                                    foreach ($photos as $photo) {
                                        $width = $photo->width;
//                                $height = $photo->height;
                                        $photo_reference = $photo->photo_reference;

                                        $data_result->venue_image = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=' . $width . '&photoreference=' . $photo_reference . '&key=' . $google_api_key;

                                        break; //capture only the first object
                                    }
                                }
                                $data_result->last_sync_at = Carbon::now()->toDateTimeString();
                                $data_result->update();
                            } //if body found
                        } // if status == 200
                    } catch (\Exception $e) {
                        print_r($e);
                    }
                } else {
                    echo $venue[0] . "<br/>";
                }
            } else if ($content->status == "ZERO_RESULTS") {
                echo $venue[0] . "<br/>";
            }
            $this->rows++;
        }
    }

//}

    public function getRowCount(): int
    {
        return $this->rows;
    }
}
